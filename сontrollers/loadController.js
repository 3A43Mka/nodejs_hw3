const User = require('../models/user');
const Load = require('../models/load');
const Truck = require('../models/truck');
const bcrypt = require('bcryptjs');
const { Types } = require('mongoose');
const joi = require('joi');
const truck = require('../models/truck');

const sprinter_limits = {
    payload: 1700,
    width: 300,
    length: 250,
    height: 170
};

const small_straight_limits = {
    payload: 2500,
    width: 500,
    length: 250,
    height: 170
};

const large_straight_limits = {
    payload: 4000,
    width: 700,
    length: 350,
    height: 200
};

const searchSchema = joi.object({
    status: joi.string()
        .default('')
        .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'),
    limit: joi.number()
        .default(10)
        .integer()
        .min(1)
        .max(50),
    offset: joi.number()
        .default(0)
        .min(0)
        .integer()
});

const addLoadSchema = joi.object({
    name: joi.string()
        .required(),
    payload: joi.number()
        .required()
        .integer()
        .min(1),
    pickup_address: joi.string()
        .required(),
    delivery_address: joi.string()
        .required(),
    dimensions: joi.object({
        width: joi.number()
            .integer()
            .min(1),
        length: joi.number()
            .integer()
            .min(1),
        height: joi.number()
            .integer()
            .min(1)
    })
});

const updateLoadSchema = joi.object({
    name: joi.string(),
    payload: joi.number()
        .integer()
        .min(1),
    pickup_address: joi.string(),
        delivery_address: joi.string(),
    dimensions: joi.object({
        width: joi.number()
            .integer()
            .min(1),
        length: joi.number()
            .integer()
            .min(1),
        height: joi.number()
            .integer()
            .min(1)
    })
});

module.exports.getLoads = async (req, res) => {
    try {
        const { status, limit, offset } = joi.attempt(req.query, searchSchema)
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "Your profile doesn't exist" });
        }
        console.log("Loads were requested");
        if (gotUser.role == "SHIPPER") {
            let query = {};
            query.created_by = gotUser._id;
            status ? query.status = status : null;

            let loads = await Load.find(query).skip(offset).limit(limit);
            return res.json({ loads: loads });
        } else {
            let query = {};
            query.assigned_to = gotUser._id;
            status ? query.status = status : null;
            let loads = await Load.find(query).skip(offset).limit(limit);
            return res.json({ loads: loads });
        }
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.addLoad = async (req, res) => {
    try {
        const { name, payload, pickup_address, delivery_address, dimensions } = joi.attempt(req.body, addLoadSchema)
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "Your profile doesn't exist" });
        }
        if (gotUser.role != "SHIPPER") {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        const newLoad = new Load({ name, payload, pickup_address, delivery_address, dimensions, created_by: req.user._id, created_date: Date.now(), status: "NEW" });
        await newLoad.save();
        console.log(`New load ${name} was added`);
        return res.json({ message: "Load created successfully" });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.loadDetails = async (req, res) => {
    try {
        const { loadId } = req.params;
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "User doesn't exist" });
        }
        if (!Types.ObjectId.isValid(loadId)) {
            return res.status(400).json({ message: 'invalid load id' });
        }
        const load = await Load.findById(loadId);
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        console.log(`Load deatails of ${loadId} was requested`);
        if (gotUser.role == "SHIPPER") {
            if (load.created_by != req.user._id) {
                return res.status(403).json({ message: "Unauthorized for this action" });
            }
            return res.json({ load });
        } else {
            if (load.assigned_to != req.user._id) {
                return res.status(403).json({ message: "Unauthorized for this action" });
            }
            return res.json({ load });
        }
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t get load, some error happened' });
    }
};

module.exports.postLoad = async (req, res) => {
    try {
        const { loadId } = req.params;
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "User doesn't exist" });
        }
        if (gotUser.role != 'SHIPPER') {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        if (!Types.ObjectId.isValid(loadId)) {
            return res.status(400).json({ message: 'invalid load id' });
        }
        let load = await Load.findById(loadId);
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        if (load.status != "NEW") {
            return res.status(409).json({ message: 'Load is already in progress' });
        }
        let trucks = await Truck.find({ status: "IS" });
        if (trucks.length == 0) {
            await Load.findByIdAndUpdate(load._id, {
                "$push":
                {
                    "logs":
                    {
                        "message": `Load couldn't be assigned: no trucks available`,
                        "time": Date.now()
                    }
                }
            });
            return res.json({ message: "Load couldn't be posted: no trucks available", driver_found: false });
        }
        for (t of trucks) {
            if (t.type == "SPRINTER") {
                if (load.payload < sprinter_limits.payload &&
                    load.dimensions.width < sprinter_limits.width &&
                    load.dimensions.length < sprinter_limits.length &&
                    load.dimensions.height < sprinter_limits.height) {
                    await Truck.findByIdAndUpdate(t._id, { status: "OL" });
                    await Load.findByIdAndUpdate(load._id, {
                        status: "ASSIGNED", state: "En route to Pick Up", assigned_to: t.assigned_to, "$push":
                        {
                            "logs":
                            {
                                "message": `Load assigned to driver with id ${t.assigned_to}`,
                                "time": Date.now()
                            }
                        }
                    });
                    console.log(`Load ${loadId} was posted`);
                    return res.json({ message: "Load posted successfully", driver_found: true });
                }
            } else if (t.type == "SMALL STRAIGHT") {
                if (load.payload < small_straight_limits.payload &&
                    load.dimensions.width < small_straight_limits.width &&
                    load.dimensions.length < small_straight_limits.length &&
                    load.dimensions.height < small_straight_limits.height) {
                        await Truck.findByIdAndUpdate(t._id, { status: "OL" });
                        await Load.findByIdAndUpdate(load._id, {
                        status: "ASSIGNED", state: "En route to Pick Up", assigned_to: t.assigned_to, "$push":
                        {
                            "logs":
                            {
                                "message": `Load assigned to driver with id ${t.assigned_to}`,
                                "time": Date.now()
                            }
                        }
                    });
                    console.log(`Load ${loadId} was posted`);
                    return res.json({ message: "Load posted successfully", driver_found: true });
                }
            } else if (t.type == "LARGE STRAIGHT") {
                if (load.payload < large_straight_limits.payload &&
                    load.dimensions.width < large_straight_limits.width &&
                    load.dimensions.length < large_straight_limits.length &&
                    load.dimensions.height < large_straight_limits.height) {
                        await Truck.findByIdAndUpdate(t._id, { status: "OL" });
                        await Load.findByIdAndUpdate(load._id, {
                        status: "ASSIGNED", state: "En route to Pick Up", assigned_to: t.assigned_to, "$push":
                        {
                            "logs":
                            {
                                "message": `Load assigned to driver with id ${t.assigned_to}`,
                                "time": Date.now()
                            }
                        }
                    });
                    console.log(`Load ${loadId} was posted`);
                    return res.json({ message: "Load posted successfully", driver_found: true });
                }
            } else {
                return res.status(500).json({ message: 'Wrong truck type' });
            }
            await Load.findByIdAndUpdate(load._id, {
                "$push":
                {
                    "logs":
                    {
                        "message": `Load couldn't be assigned: no fitting trucks`,
                        "time": Date.now()
                    }
                }
            });
            return res.json({ message: "Load couldn't be posted: no fitting truck found", driver_found: false });
        }
    } catch (err) {
        console.log(err);
        return res.status(500).json({ message: err.message });
    }
};

module.exports.activeLoad = async (req, res) => {
    try {
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "User doesn't exist" });
        }
        if (gotUser.role !== "DRIVER") {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }

        const load = await Load.findOne({ status: "ASSIGNED", assigned_to: req.user._id });
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        console.log(`active load for ${req.user._id} was requested`);
        return res.json({ load });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t get load, some error happened' });
    }
};

module.exports.iterateState = async (req, res) => {
    try {
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "User doesn't exist" });
        }
        if (gotUser.role !== "DRIVER") {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        let load = await Load.findOne({ status: "ASSIGNED", assigned_to: req.user._id });
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        if (load.assigned_to != req.user._id) {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        console.log(`Load state for ${load._id} has changed`);
        if (load.state == 'En route to Pick Up') {
            await Load.findOneAndUpdate({_id :load._id}, {
                state: 'Arrived to Pick Up',
                "$push":
                {
                    "logs":
                    {
                        "message": `Load state changed to 'Arrived to Pick Up`,
                        "time": Date.now()
                    }
                }
            });
            return res.json({ message: `Load state changed to 'Arrived to Pick Up'` });
        } else if (load.state == 'Arrived to Pick Up') {
            await Load.findOneAndUpdate({_id :load._id}, {
                state: 'En route to delivery',
                "$push":
                {
                    "logs":
                    {
                        "message": `Load state changed to 'En route to delivery`,
                        "time": Date.now()
                    }
                }
            });
            return res.json({ message: `Load state changed to 'En route to delivery'` });
        } else if (load.state == 'En route to delivery') {
            await Load.findOneAndUpdate({_id :load._id}, {
                state: 'Arrived to delivery', status: "SHIPPED",
                "$push":
                {
                    "logs":
                    {
                        "message": `Load state changed to 'Arrived to delivery`,
                        "time": Date.now()
                    }
                }
            });
            await Truck.findOneAndUpdate({ status: "OL", assigned_to: load.assigned_to }, { status: "IS" });
            return res.json({ message: `Load state changed to 'Arrived to delivery'` });
        }
    } catch (err) {
        return res.status(500).json({ message: err });
    }
};

module.exports.updateLoad = async (req, res) => {
    try {
        const { loadId } = req.params;
        const { name, payload, pickup_address, delivery_address, dimensions } = joi.attempt(req.body, updateLoadSchema)
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "Your profile doesn't exist" });
        }
        if (gotUser.role != "SHIPPER") {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        if (!Types.ObjectId.isValid(loadId)) {
            return res.status(400).json({ message: 'invalid load id' });
        }
        const load = await Load.findById(loadId);
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        if (load.created_by != req.user._id) {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        if (load.status != "NEW"){
            return res.status(400).json({ message: "Load is already in progress" });
        }
        if (name){
            await Load.findOneAndUpdate({_id: load._id}, {name: name});
        }
        if (payload){
            await Load.findOneAndUpdate({_id: load._id}, {payload: payload});
        }
        if (pickup_address) {
            await Load.findOneAndUpdate({_id: load._id}, {pickup_address: pickup_address});
        }
        if (delivery_address) {
            await Load.findOneAndUpdate({_id: load._id}, {delivery_address: delivery_address});
        }
        if (dimensions){
            await Load.findOneAndUpdate({_id: load._id}, {dimensions: dimensions});
        }
        console.log(`Load ${loadId} was updated`);
        return res.json({ message: "Load updated successfully" });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.deleteLoad = async (req, res) => {
    try {
        const { loadId } = req.params;
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "Your profile doesn't exist" });
        }
        if (gotUser.role != "SHIPPER") {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        if (!Types.ObjectId.isValid(loadId)) {
            return res.status(400).json({ message: 'invalid load id' });
        }
        const load = await Load.findById(loadId);
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        if (load.created_by != req.user._id) {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        if (load.status != "NEW"){
            return res.status(400).json({ message: "Load is already in progress" });
        }
        await newLoad.findByIdAndDelete(loadId);
        console.log(`Load ${loadId} was deleted`);
        return res.json({ message: "Load deleted successfully" });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.shippingInfo = async (req, res) => {
    try {
        const { loadId } = req.params;
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "User doesn't exist" });
        }
        if (gotUser.role != "SHIPPER") {
            return res.status(403).json({ message: "Unauthorized for this action" });
        }
        if (!Types.ObjectId.isValid(loadId)) {
            return res.status(400).json({ message: 'invalid load id' });
        }
        const load = await Load.findById(loadId);
        if (!load) {
            return res.status(404).json({ message: 'Load not found' });
        }
        if (load.status == "NEW"){
            return res.status(400).json({ message: 'No shipment for this load' });
        }
        const truck = await Truck.findOne({assigned_to: load.assigned_to});
        if (!truck) {
            return res.status(400).json({ message: 'Truck not found' });
        }
        console.log(`Shipment info for ${loadId} was requested`);
        return res.json({load, truck});
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t get load, some error happened' });
    }
};