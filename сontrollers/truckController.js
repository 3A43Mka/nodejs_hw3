const User = require('../models/user');
const Truck = require('../models/truck');
const {Types} = require('mongoose');
const joi = require('joi');
const bcrypt = require('bcryptjs');

const createTruckSchema = joi.object({
    type: joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required()
});

module.exports.getTrucks = async (req, res) => {
    try {
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "User doesn't exist"});
        }
        if (gotUser.role != 'DRIVER'){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        const trucks = await Truck.find({created_by: req.user._id});
        console.log(`Trucks were requested`);
        return res.json({trucks: trucks});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t get trucks, some error happened'});
    }
};

module.exports.addTruck = async (req, res) => {
    try {
        const {type} = joi.attempt(req.body, createTruckSchema);
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "User doesn't exist"});
        }
        if (gotUser.role != 'DRIVER'){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        const newTruck = new Truck({type: type, status: 'OS', created_by: req.user._id, created_date: Date.now()});
        await newTruck.save();
        console.log(`New truck was created`);
        return res.json({message: "Truck created successfully"});
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.truckDetails = async (req, res) => {
    try {
        const {truckId} = req.params;
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "User doesn't exist"});
        }
        if (gotUser.role != 'DRIVER'){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        if (!Types.ObjectId.isValid(truckId)){
            return res.status(400).json({ message: 'invalid truck id' });
        }
        const truck = await Truck.findById(truckId);
        if (!truck){
            return res.status(404).json({ message: 'Truck not found' });
        }
        if (truck.created_by != req.user._id){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        console.log(`Details for truck ${truckId} were requested`);
        return res.json({truck});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t get truck, some error happened'});
    }
};

module.exports.updateTruck = async (req, res) => {
    try {
        const {truckId} = req.params;
        const {type} = joi.attempt(req.body, createTruckSchema);
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "User doesn't exist"});
        }
        if (gotUser.role != "DRIVER"){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        if (!Types.ObjectId.isValid(truckId)){
            return res.status(400).json({ message: 'invalid truck id' });
        }
        const truck = await Truck.findById(truckId);
        if (!truck){
            return res.status(404).json({ message: 'Truck not found' });
        }
        if (truck.created_by != req.user._id){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        if (truck.status == "OL"){
            return res.status(404).json({ message: 'Truck is on load' });
        }
        await Truck.findByIdAndUpdate(truckId, {type : type});
        console.log(`Truck ${truckId} was successfully updated`);
        return res.json({message:"Truck details changed successfully"});
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.deleteTruck = async (req, res) => {
    try {
        const {truckId} = req.params;
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "User doesn't exist"});
        }
        if (gotUser.role != "DRIVER"){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        if (!Types.ObjectId.isValid(truckId)){
            return res.status(400).json({ message: 'invalid truck id' });
        }
        const truck = await Truck.findById(truckId);
        if (!truck){
            return res.status(404).json({ message: 'Truck not found' });
        }
        if (truck.created_by != req.user._id){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        if (truck.status == "OL"){
            return res.status(404).json({ message: 'Truck is on load' });
        }
        await Truck.findByIdAndDelete(truckId);
        console.log(`Truck ${truckId} was deleted`);
        return res.json({message:"Truck was successfully deleted"});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t delete truck, some error happened'});
    }
};

module.exports.assignTruck = async (req, res) => {
    try {
        const {truckId} = req.params;
        let gotUser = await User.findOne({_id: req.user._id});
        if (!gotUser) {
            return res.status(400).json({message: "User doesn't exist"});
        }
        if (gotUser.role != "DRIVER"){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        const isOnLoad = await Truck.find({created_by: req.user._id, status: "OL"});
        if (isOnLoad.length>0){
            return res.status(400).json({message: "Driver already has a load"});
        }
        if (!Types.ObjectId.isValid(truckId)){
            return res.status(400).json({ message: 'invalid truck id' });
        }
        const truck = await Truck.findById(truckId);
        if (!truck){
            return res.status(404).json({ message: 'Truck not found' });
        }
        if (truck.created_by != req.user._id){
            return res.status(403).json({message: "Unauthorized for this action"});
        }
        if (truck.status == "OL"){
            return res.status(404).json({ message: 'Truck is on load' });
        }
        await Truck.updateMany({status: "IS"}, {assigned_to : undefined, status: "OS"});
        await Truck.findByIdAndUpdate(truckId, {assigned_to: req.user._id, status: "IS"});
        console.log(`Truck ${truckId} was assigned`);
        return res.json({message:"Truck assigned successfully"});
    } catch (err) {
        return res.status(500).json({message: 'Couldn\'t assign truck, some error happened'});
    }
};