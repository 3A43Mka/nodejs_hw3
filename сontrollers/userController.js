const User = require('../models/user');
const Truck = require('../models/truck');
const bcrypt = require('bcryptjs');
const joi = require('joi');

const changePasswordSchema = joi.object({
    oldPassword: joi.string()
        .required()
        .min(4),
        newPassword: joi.string()
        .required()
        .min(4)
});

module.exports.getProfile = async (req, res) => {
    try {
        let gotUser = await User.findOne({ _id: req.user._id });
        if (!gotUser) {
            return res.status(400).json({ message: "Requested profile doesn't exist" });
        }
        gotUser.role = undefined;
        gotUser.password = undefined;
        console.log(`Profile ${req.user._id} was requested`);
        return res.json({ user: gotUser });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t get profile, some error happened' });
    }
};

module.exports.deleteProfile = async (req, res) => {
    try {
        const isOnLoad = await Truck.find({created_by: req.user._id, status: "OL"});
        if (isOnLoad.length>0){
            return res.status(400).json({message: "Driver can't delete profile while he has a load"});
        }
        await User.findOneAndDelete({ _id: req.user._id });
        console.log(`Profile ${req.user._id} was deleted`);
        return res.json({ message: 'Success' });
    } catch (err) {
        return res.status(500).json({ message: 'Couldn\'t delete profile, some error happened' });
    }
};

module.exports.changePassword = async (req, res) => {
    try {
        const { oldPassword, newPassword } = joi.attempt(req.body, changePasswordSchema);
        let user = await User.findOne({ _id: req.user._id });
        if (!user) {
            return res.status(400).json({ message: "Requested profile doesn't exist" });
        }
        let doPasswordsMatch = await bcrypt.compare(oldPassword, user.password);
        if (!doPasswordsMatch) {
            return res.status(400).json({ message: "Wrong old password" });
        }
        let hash = await bcrypt.hash(newPassword, 3);
        await User.findOneAndUpdate({ _id: req.user._id }, { password: hash });
        console.log(`Password for ${user._id} was changed`);
        return res.json({ message: 'Success' });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};