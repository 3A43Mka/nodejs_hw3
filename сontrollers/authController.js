const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('config');
const bcrypt = require('bcryptjs');
const joi = require('joi');
const secret = config.get('secret');
const nodemailer = require("nodemailer");

const registerCredsSchema = joi.object({
    email: joi.string()
        .email()
        .required(),
    password: joi.string()
        .min(4)
        .required(),
    role: joi.string()
        .valid('DRIVER', 'SHIPPER')
        .required()
});

const loginCredsSchema = joi.object({
    email: joi.string()
        .email()
        .required(),
    password: joi.string().
        required()
        .min(4),
});

const forgotPasswordSchema = joi.object({
    email: joi.string()
        .email()
        .required()
});

module.exports.register = async (req, res) => {
    try {
        const { email, password, role } = joi.attempt(req.body, registerCredsSchema);
        const gotUser = await User.findOne({ email: email });
        if (gotUser) {
            return res.status(400).json({ message: 'User already exists' });
        }
        const hash = await bcrypt.hash(password, 3);
        const user = new User({ email, password: hash, created_date: Date.now(), role });
        await user.save();
        console.log(`user ${email} was registered`);
        return res.json({ message: 'Success' });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.login = async (req, res) => {
    try {
        const { email, password } = joi.attempt(req.body, loginCredsSchema);
        let user = await User.findOne({ email }).exec();
        if (!user) {
            return res.status(400).json({ message: 'No user with that username and password found' });
        }
        let gotUser = { _id: user._id, role: user.role, email: user.email, created_date: user.created_date };
        let isPasswordCorrect = await bcrypt.compare(password, user.password);
        if (!isPasswordCorrect) {
            return res.status(400).json({ message: 'Wrong password' });
        }
        console.log(`user ${email} has logged in`);
        return res.json({ jwt_token: jwt.sign(JSON.stringify(gotUser), secret) });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};

module.exports.forgot_password = async (req, res) => {
    try {
        const { email } = joi.attempt(req.body, forgotPasswordSchema);
        let user = await User.findOne({ email }).exec();
        if (!user) {
            return res.status(400).json({ message: 'No user with that username found' });
        }
        const newPass = Math.random().toString(36).slice(-8).toString();
        const newHash = await bcrypt.hash(newPass, 3);
        await User.findOneAndUpdate({ email }, { password: newHash });

        let transporter = nodemailer.createTransport({
            service: "Gmail",
            auth: {
                user: "iforgotmypasspleasehelp@gmail.com",
                pass: "111223111223",
            },
        });

        let info = await transporter.sendMail({
            from: 'iforgotmypasspleasehelp@gmail.com', 
            to: email, 
            subject: "Password change", 
            text: `Your new password is ${newPass}`, 
            html: `Your new password is ${newPass}`, 
        });

        console.log(`user ${email} has restored password`);
        return res.json({ message: "New password sent to your email address" });
    } catch (err) {
        if (err.isJoi) {
            const message = err.details.map(e => e.message).join('\n');
            res.status(400).json({ message: message });
        }
        else {
            console.log(err);
            res.status(500).json({ message: "Server error" });
        }
    }
};