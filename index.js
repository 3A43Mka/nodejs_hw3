const express = require('express');
const mongoose = require('mongoose');
const config = require('config');

const app = express();

const port = process.env.PORT || config.get('port') || 8080;
const dblogin = config.get('dblogin');
const dbpass = config.get('dbpass');
const dbname = config.get('dbname');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

mongoose.connect(`mongodb+srv://${dblogin}:${dbpass}@cluster0.shoui.mongodb.net/${dbname}?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(express.json());

app.use('/api/', userRouter);
app.use('/api/', authRouter);
app.use('/api/', truckRouter);
app.use('/api/', loadRouter);


app.use((req, res) => {
    res.status(404).json({message: 'Route doesn\'t exist'});
});
app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});