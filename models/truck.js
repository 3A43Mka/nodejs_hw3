const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Types = mongoose.Types;

module.exports = mongoose.model('truck', new Schema({

    created_by: {
        type: Types.ObjectId, ref: 'User', 
        required: true
    },
    assigned_to: {
        type: Types.ObjectId, ref: 'User',
    },
    type: {
        type: String,
        required: true,
        enum: [ 'SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT' ]
    },
    status: {
        type: String,
        required: true,
        enum: ['OS', 'OL', 'IS']
    },
    created_date: {
        type: Date, 
        required: true
    }
}, {versionKey: false}));