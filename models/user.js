const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('user', new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ['DRIVER','SHIPPER'],
        required: true
    },
    created_date: {
        type: Date, 
        required: true
    }
}, {versionKey: false}));