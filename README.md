**Homework 3**

Homework №3, rest API for shipment management

**Requirements**

[Node.js](https://nodejs.org/en/) - to run server.

**How to run**

Run this commands in terminal:

1. Clone this repository with git clone 
2. Install packages `npm install`
3. Run with `npm run start`
4. Enjoy