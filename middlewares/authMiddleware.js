const jwt = require('jsonwebtoken');
const config = require('config');
const secret = config.get('secret');

module.exports = (request, response, next) => {
    const authHeader = request.headers['authorization'];
    if (!authHeader) {
        return response.status(401).json({message: 'No authorization header found'});
    }

    const [, jwtToken] = authHeader.split(' ');
    try {
        request.user = jwt.verify(jwtToken, secret);
        next();
    } catch (err) {
        return response.status(401).json({message: 'Invalid JWT'});
    }
};