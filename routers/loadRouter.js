const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {getLoads, addLoad, loadDetails, postLoad, activeLoad, iterateState, updateLoad, deleteLoad, shippingInfo} = require('../сontrollers/loadController');

router.get('/loads', authMiddleware, getLoads);
router.post('/loads', authMiddleware, addLoad);
router.get('/loads/active', authMiddleware, activeLoad);
router.patch('/loads/active/state', authMiddleware, iterateState);
router.get('/loads/:loadId', authMiddleware, loadDetails);
router.put('/loads/:loadId', authMiddleware, updateLoad);
router.delete('/loads/:loadId', authMiddleware, deleteLoad);
router.post('/loads/:loadId/post', authMiddleware, postLoad);
router.get('/loads/:loadId/shipping_info', authMiddleware, shippingInfo);

module.exports = router;