const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {getProfile, deleteProfile, changePassword} = require('../сontrollers/userController');

router.get('/users/me', authMiddleware, getProfile);
router.delete('/users/me', authMiddleware, deleteProfile);
router.patch('/users/me/password', authMiddleware, changePassword);

module.exports = router;