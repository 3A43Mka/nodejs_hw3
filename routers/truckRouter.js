const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {getTrucks, addTruck, truckDetails, updateTruck, deleteTruck, assignTruck} = require('../сontrollers/truckController');

router.get('/trucks', authMiddleware, getTrucks);
router.post('/trucks', authMiddleware, addTruck);
router.get('/trucks/:truckId', authMiddleware, truckDetails);
router.put('/trucks/:truckId', authMiddleware, updateTruck);
router.delete('/trucks/:truckId', authMiddleware, deleteTruck);
router.post('/trucks/:truckId/assign', authMiddleware, assignTruck);

module.exports = router;