**Auth:**

* POST /api/auth/register
  * Acceptable request body:
    ```json
    {
        "email": "kyrylo@gmail.com",
        "password": "ve518dl3",
        "role": "SHIPPER"
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "Profile created successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* POST /api/auth/login
  * Acceptable request body:
    ```json
    {
        "email": "kyrylo@gmail.com",
        "password": "ve518dl3"
    }
    ```
      
  * Success response:
    ```json
    {
        "jwt_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0.yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* POST /api/auth/forgot_password
  * Acceptable request body:
    ```json
    {
        "email": "kyrylo@gmail.com"
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "New password sent to your email address"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

**User:**

* GET /api/users/me
  * Success response:
    ```json
    {
        "user": {
            "_id": "5fb5942a65f1f30fe81c1a9d",
            "email": "dima@gmail.com", 
            "created_date": "2020-11-18T21:37:46.504Z"
        }
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* DELETE /api/users/me
  * Success response:
    ```json
    {
        "message": "Profile deleted successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* PATCH /api/users/me
  * Acceptable request body:
    ```json
    {
        "oldPassword": "string",
        "newPassword": "string"
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "Password changed successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

**Truck:**

* GET /api/trucks
  * Success response:
    ```json
    {
        "trucks": [
            {
                "_id": "5099803df3f4948bd2f98391",
                "created_by": "5099803df3f4948bd2f98391",
                "assigned_to": "5099803df3f4948bd2f98391",
                "type": "SPRINTER",
                "status": "IS",
                "created_date": "2020-10-28T08:03:19.814Z"
            }
        ]
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* POST /api/trucks
  * Acceptable request body:
    ```json
    {
        "type": "LARGE STRAIGHT"
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "Truck created successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* GET /api/trucks/{id}
  * Success response:
    ```json
    {
        "truck": {
            "_id": "5099803df3f4948bd2f98391",
            "created_by": "5099803df3f4948bd2f98391",
            "assigned_to": "5099803df3f4948bd2f98391",
            "type": "SPRINTER",
            "status": "IS",
            "created_date": "2020-10-28T08:03:19.814Z"
        }
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* PUT /api/trucks/{id}
  * Acceptable request body:
    ```json
    {
        "type": "LARGE STRAIGHT"
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "Truck details changed successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* DELETE /api/trucks/{id}
  * Success response:
    ```json
    {
        "message": "Truck deleted successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* POST /api/trucks/{id}/assign
  * Success response:
    ```json
    {
        "message": "Truck assigned successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

**Load:**

* GET /api/loads
  * Query parameters:
    * status - NEW, POSTED, ASSIGNED, SHIPPED;
    * limit - integer from 1 to 50;
    * offset - integer, more or equal to 0;
  * Success response:
    ```json
    {
        "loads": [
            {
                "_id": "5099803df3f4948bd2f98391",
                "created_by": "5099803df3f4948bd2f98391",
                "assigned_to": "5099803df3f4948bd2f98391",
                "status": "NEW",
                "state": "En route to Pick Up",
                "name": "Moving sofa",
                "payload": 100,
                "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
                "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
                "dimensions": {
                    "width": 44,
                    "length": 32,
                    "height": 66
                },
                "logs": [
                    {
                        "message": "Load assigned to driver with id ###",
                        "time": "2020-10-28T08:03:19.814Z"
                    }
                ],
                "created_date": "2020-10-28T08:03:19.814Z"
            }
        ]
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* POST /api/loads
  * Acceptable request body:
    ```json
    {
        "name": "Moving sofa 2",
        "payload": 100,
        "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
        "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
        "dimensions": {
            "width": 44,
            "length": 32,
            "height": 66
        }
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "Load created successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* GET /api/loads/active
  * Success response:
    ```json
    {
        "load": {
        "dimensions": {
        "width": 50,
        "length": 50,
        "height": 50
        },
        "_id": "5fb6f048e66dba4c640963fc",
        "name": "Moving sofa",
        "payload": 3000,
        "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
        "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
        "logs": [
            {
                "_id": "5fb83f0666594a19c018a95c",
                "message": "Driver not found",
                "time": "2020-11-20T22:11:18.565Z"
            }
        ],
        "created_date": "2020-11-19T22:23:04.136Z",
        "created_by": "5fb567547f2afd4cd8bbda7a",
        "assigned_to": "5fb5942a65f1f30fe81c1a9d",
        "status": "ASSIGNED",
        "state": "En route to Pick Up"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```
    
* PATCH /api/loads/active/state
  * Success response:
    ```json
    {
        "message": "Load state changed to 'En route to Delivery'"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```
    
* GET /api/loads/{id} 
  * Success response:
    ```json
    {
        "load": {
            "_id": "5099803df3f4948bd2f98391",
            "created_by": "5099803df3f4948bd2f98391",
            "assigned_to": "5099803df3f4948bd2f98391",
            "status": "NEW",
            "state": "En route to Pick Up",
            "name": "Moving sofa",
            "payload": 100,
            "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
            "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
            "dimensions": {
                "width": 44,
                "length": 32,
                "height": 66
            },
            "logs": [
            {
                "message": "Load assigned to driver with id ###",
                "time": "2020-10-28T08:03:19.814Z"
            }
            ],
            "created_date": "2020-10-28T08:03:19.814Z"
        }
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* PUT /api/loads/{id}
  * Acceptable request body:
    ```json
    {
        "name": "Moving sofa",
        "payload": 100,
        "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
        "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
        "dimensions": {
            "width": 44,
            "length": 32,
            "height": 66
        }
    }
    ```
      
  * Success response:
    ```json
    {
        "message": "Load details changed successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* DELETE /api/loads/{id}
  * Success response:
    ```json
    {
        "message": "Load deleted successfully"
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* POST /api/loads/{id}/post
  * Success response:
    ```json
    {
        "message": "Load posted successfully",
        "driver_found": true
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```

* GET /api/loads/{id}/shipping_info
  * Success response:
    ```json
    {
        "load": {
            "_id": "5099803df3f4948bd2f98391",
            "created_by": "5099803df3f4948bd2f98391",
            "assigned_to": "5099803df3f4948bd2f98391",
            "status": "NEW",
            "state": "En route to Pick Up",
            "name": "Moving sofa",
            "payload": 100,
            "pickup_address": "Flat 25, 12/F, Acacia Building 150 Kennedy Road",
            "delivery_address": "Sr. Rodrigo Domínguez Av. Bellavista N° 185",
            "dimensions": {
                "width": 44,
                "length": 32,
                "height": 66
            },
            "logs": [
                {
                    "message": "Load assigned to driver with id ###",
                    "time": "2020-10-28T08:03:19.814Z"
                }
            ],
            "created_date": "2020-10-28T08:03:19.814Z"
        },
        "truck": {
            "_id": "5099803df3f4948bd2f98391",
            "created_by": "5099803df3f4948bd2f98391",
            "assigned_to": "5099803df3f4948bd2f98391",
            "type": "SPRINTER",
            "status": "IS",
            "created_date": "2020-10-28T08:03:19.814Z"
        }
    }
    ```
    
  * Error response:
    ```json
    {
        "message": "string"
    }
    ```